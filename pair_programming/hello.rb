=begin
doctest: I can greet the world
>> hello
=> "Hello World!"
doctest: I can greet someone by name
>> hello 'Kevin'
=> "Hello Kevin!"
doctest: I can greet someone else by name
>> hello 'Victor'
=> "Hello Victor!"
doctest: I can ask if someone is there
>> hello 'Santa', '?'
>> "Hello Santa?"
=end

def hello(name='World', punctuation='!')
  "Hello #{name}#{punctuation}"
end
