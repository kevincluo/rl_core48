# RubyLearning Core Course Batch 48

Working through this course with Victor.  Should be fun.

## My Environment

I am using RVM with Ruby 1.9.3-p484.

I will start using Rubydoctest gem for testing.  As I don't know Ruby,
rubydoctest allows me to use IRB as tests, which is nice, as I should be
using IRB to explore Ruby anyway.

I may move on to using minitest, which is built in to Ruby, so a logical
2nd step for testing.  Once I know some Ruby, then it makes sense to use
something more complicated than simply IRB as tests.
